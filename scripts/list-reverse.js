//Funcion para almacenar la palabra en una lista para ser mostrada luego
var palabras=[];
document.getElementById("resultado").style.display = "none";
document.getElementById("alerta-success").style.display = "none";
document.getElementById("alerta-error").style.display = "none";

function guardarInformacion(){
    if(document.getElementById("palabra").value == ""){     
        document.getElementById("msgError").innerHTML = "No se ha escrito nada";   
        document.getElementById("alerta-error").style.display = "block";
    }else{
        palabras.push(document.getElementById("palabra").value);
        document.getElementById("msgSuccess").innerHTML = "Se almaceno la palabra: "+document.getElementById("palabra").value;   
        document.getElementById("alerta-success").style.display = "block";
    }
    document.getElementById("palabra").value = "";
}

//Funcion para limpiar lista 
function limpiarLista(){
    document.getElementById("resultado").style.display = "none";
    palabras = [];
    document.getElementById("palabrasNormales").innerHTML = "";
    document.getElementById("palabrasRevertidas").innerHTML = "";
}

//Funcion para mostrar la lista de manera inversa
function mostrarLista(){
    var lonArrPalabras = palabras.length -1;
    //Muestra las palabras sin revertirlas aún
    for(var index= 0; index <= (palabras.length -1); index ++){
        if(document.getElementById("palabrasNormales").innerHTML == ""){
            document.getElementById("palabrasNormales").innerHTML = 
            document.getElementById("palabrasNormales").innerHTML+ palabras[index];
        }else{
            document.getElementById("palabrasNormales").innerHTML = 
            document.getElementById("palabrasNormales").innerHTML + ", " + palabras[index];
        }
    }
    for(var index=0; index <= ((palabras.length -1) / 2) ; index++){
        inversion(palabras[index], palabras[lonArrPalabras - index], index, (lonArrPalabras - index));
    }  
    //Muestra las palabras ya revertidas
    for(var index= 0; index <= (palabras.length -1); index ++){
        if(document.getElementById("palabrasRevertidas").innerHTML == ""){
            document.getElementById("palabrasRevertidas").innerHTML = 
            document.getElementById("palabrasRevertidas").innerHTML+ palabras[index];
        }else{
            document.getElementById("palabrasRevertidas").innerHTML = 
            document.getElementById("palabrasRevertidas").innerHTML + ", " + palabras[index];
        }
    }
    document.getElementById("resultado").style.display = "block";
}

//Funcion para invertir la lista
function inversion(primerValor, segundoValor, primeraPosicion, segundaPosicion){
    palabras[primeraPosicion] = segundoValor;
    palabras[segundaPosicion] = primerValor;
}