var secuencia=[];
var numSig;
var cont = 0;
var time;
var verMostrar = false;
var siguColor;
var contJuego = 0;
var audio1 = document.getElementById("audio1");
var audio2 = document.getElementById("audio2");
var audio3 = document.getElementById("audio3");
var audio4 = document.getElementById("audio4");

function comenzar(){
    document.getElementById("mensaje").innerHTML="Espera...";
    numSig = Math.trunc(Math.random() * (5 - 1) + 1);
    secuencia.push(numSig);
    verMostrar = false;
    document.getElementById("comenzar").disabled= true;
    time = setInterval('colores()', 1300);
}

function colores(){
    if(cont == 0){
        siguColor = secuencia[cont];
    }
    if(secuencia[cont] == 1){
        document.getElementById("colorRojo").style.background = "#d50000";
        audio1.play();
        setTimeout(function(){
            document.getElementById("colorRojo").style.background = "#ff8a80";
        }, 800);
    }
    if(secuencia[cont] == 2){
        document.getElementById("colorVerde").style.background = "#00c853";
        audio2.play();
        setTimeout(function(){
            document.getElementById("colorVerde").style.background = "#b9f6ca";
        }, 800);
    }
    if(secuencia[cont] == 3){
        document.getElementById("colorAzul").style.background = "#0d47a1";
        audio3.play();
        setTimeout(function(){
            document.getElementById("colorAzul").style.background = "#bbdefb";
        }, 800);
    }
    if(secuencia[cont] == 4){
        document.getElementById("colorAmarillo").style.background = "#ffea00";
        audio4.play();
        setTimeout(function(){
            document.getElementById("colorAmarillo").style.background = "#fff9c4";
        }, 800);
    }
    cont ++;
    if(secuencia.length == cont){
        cont = 0;
        console.log("fin");
        clearInterval(time);
        setTimeout(function(){
            document.getElementById("mensaje").innerHTML="Tu turno";
            verMostrar = true;
        }, 1000)
    }
}

function verColor(numero){
    if(verMostrar == true){
        if(numero == 1){
            document.getElementById("colorRojo").style.background = "#d50000";
            audio1.play();
            setTimeout(function(){
                document.getElementById("colorRojo").style.background = "#ff8a80";
            }, 400);
        }else if(numero == 2){
            document.getElementById("colorVerde").style.background = "#00c853";
            audio2.play();
            setTimeout(function(){
                document.getElementById("colorVerde").style.background = "#b9f6ca";
            }, 400);
        }else if(numero == 3){
            document.getElementById("colorAzul").style.background = "#0d47a1";
            audio3.play();
            setTimeout(function(){
                document.getElementById("colorAzul").style.background = "#bbdefb";
            }, 400);
        }else{
            document.getElementById("colorAmarillo").style.background = "#ffea00";
            audio4.play();
            setTimeout(function(){
                document.getElementById("colorAmarillo").style.background = "#fff9c4";
            }, 400);
        }
        if(siguColor == numero){
            contJuego ++;
            if(secuencia.length > contJuego){
                siguColor = secuencia[contJuego];
            }else{
                ganaste();
            }
        }else{
            perdiste();
        }

    }
}

function perdiste(){
    document.getElementById("comenzar").disabled = false;
    document.getElementById("mensaje").innerHTML = "Perdiste :(";
    verMostrar = false;
    secuencia = [];
}

function ganaste(){
    document.getElementById("mensaje").innerHTML="Bien! siguiente ronda";
    contJuego = 0;
    setTimeout(function(){
        comenzar();
    }, 1000);
}

